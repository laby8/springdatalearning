package com.example.dataspring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class Start {

    private CarRepo carRepo;

    @Autowired
    public Start(CarRepo carRepo) {
        this.carRepo = carRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runExample() {
Car car = new Car("Skoda","Ci", Color.RED.toString());

        Car car1 = new Car("Skoda","Ci", Color.BLACK.toString());
carRepo.save(car);
        carRepo.save(car1);
Iterable<Car> all = carRepo.findAllByColor(Color.RED.toString());
all.forEach(System.out::println);
    }
}
